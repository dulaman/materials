<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Material
 * @author Adam Dulewicz
 *
 * @ORM\Table(name="material")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialRepository")
 */
class Material
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var GroupOfMaterials
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GroupOfMaterials", inversedBy="materials")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;
    
    /**
     * @var UnitOfMeasurement
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UnitOfMeasurement", inversedBy="materials")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     */
    private $unit;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Material
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Material
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\GroupOfMaterials $group
     * @return Material
     */
    public function setGroup(\AppBundle\Entity\GroupOfMaterials $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\GroupOfMaterials 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set unit
     *
     * @param \AppBundle\Entity\UnitOfMeasurement $unit
     * @return Material
     */
    public function setUnit(\AppBundle\Entity\UnitOfMeasurement $unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return \AppBundle\Entity\UnitOfMeasurement 
     */
    public function getUnit()
    {
        return $this->unit;
    }
}
