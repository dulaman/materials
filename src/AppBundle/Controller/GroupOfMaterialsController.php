<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\GroupOfMaterials;
use AppBundle\Form\GroupOfMaterialsType;

/**
 * GroupOfMaterials controller.
 *
 */
class GroupOfMaterialsController extends Controller
{
    /**
     * Lists all GroupOfMaterials entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $repo = $em->getRepository('AppBundle:GroupOfMaterials');
        $groupOfMaterials = $repo->findAll();
        $repo->verify();
        $repo->recover();
        $em->flush();
        
        
        return $this->render('groupofmaterials/index.html.twig', array(
            'groupOfMaterials' => $groupOfMaterials,
            'tree' => $repo->childrenHierarchy(
                null, /* starting from root nodes */
                false, /* false: load all children, true: only direct */
                array(
                    'decorate' => true,
                    'representationField' => 'title',
                    'html' => true
                ))
        ));
    }

    /**
     * Creates a new GroupOfMaterials entity.
     *
     */
    public function newAction(Request $request)
    {
        $groupOfMaterial = new GroupOfMaterials();
        $form = $this->createForm(new GroupOfMaterialsType($this->getDoctrine()->getManager()), $groupOfMaterial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('AppBundle:GroupOfMaterials');
            $repo->persistAsLastChildOf($groupOfMaterial, $groupOfMaterial->getParent());
            $em->flush();

            return $this->redirectToRoute('group_show', array('id' => $groupOfMaterial->getId()));
        }

        return $this->render('groupofmaterials/new.html.twig', array(
            'groupOfMaterial' => $groupOfMaterial,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a GroupOfMaterials entity.
     *
     */
    public function showAction(GroupOfMaterials $groupOfMaterial)
    {
        $deleteForm = $this->createDeleteForm($groupOfMaterial);

        return $this->render('groupofmaterials/show.html.twig', array(
            'groupOfMaterial' => $groupOfMaterial,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing GroupOfMaterials entity.
     *
     */
    public function editAction(Request $request, GroupOfMaterials $groupOfMaterial)
    {
        $oldParent = $groupOfMaterial->getParent();
        $deleteForm = $this->createDeleteForm($groupOfMaterial);
        $editForm = $this->createForm(new GroupOfMaterialsType($this->getDoctrine()->getManager()), $groupOfMaterial);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($oldParent !== $groupOfMaterial->getParent()) {
                $repo = $em->getRepository('AppBundle:GroupOfMaterials');
                $repo->persistAsLastChildOf($groupOfMaterial, $groupOfMaterial->getParent());
                $em->flush();
            } else {
                $em->persist($groupOfMaterial);
                $em->flush();
            }

            return $this->redirectToRoute('group_edit', array('id' => $groupOfMaterial->getId()));
        }

        return $this->render('groupofmaterials/edit.html.twig', array(
            'groupOfMaterial' => $groupOfMaterial,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a GroupOfMaterials entity.
     *
     */
    public function deleteAction(Request $request, GroupOfMaterials $groupOfMaterial)
    {
        $form = $this->createDeleteForm($groupOfMaterial);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('AppBundle:GroupOfMaterials');
            if (count($repo->getChildren($groupOfMaterial)) > 0) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Grupa zawierającej podgrupy, więc nie może zostać usunięta!');
                return $this->redirectToRoute('group_index');
            }
            
            $repo->removeFromTree($groupOfMaterial);
            $em->flush();
        }

        return $this->redirectToRoute('group_index');
    }

    /**
     * Creates a form to delete a GroupOfMaterials entity.
     *
     * @param GroupOfMaterials $groupOfMaterial The GroupOfMaterials entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GroupOfMaterials $groupOfMaterial)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('group_delete', array('id' => $groupOfMaterial->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
