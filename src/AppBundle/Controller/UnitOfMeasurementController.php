<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\UnitOfMeasurement;
use AppBundle\Form\UnitOfMeasurementType;

/**
 * UnitOfMeasurement controller.
 *
 */
class UnitOfMeasurementController extends Controller
{
    /**
     * Lists all UnitOfMeasurement entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $unitOfMeasurements = $em->getRepository('AppBundle:UnitOfMeasurement')->findAll();

        return $this->render('unitofmeasurement/index.html.twig', array(
            'unitOfMeasurements' => $unitOfMeasurements,
        ));
    }

    /**
     * Creates a new UnitOfMeasurement entity.
     *
     */
    public function newAction(Request $request)
    {
        $unitOfMeasurement = new UnitOfMeasurement();
        $form = $this->createForm('AppBundle\Form\UnitOfMeasurementType', $unitOfMeasurement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($unitOfMeasurement);
            $em->flush();

            return $this->redirectToRoute('unit_show', array('id' => $unitOfMeasurement->getId()));
        }

        return $this->render('unitofmeasurement/new.html.twig', array(
            'unitOfMeasurement' => $unitOfMeasurement,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UnitOfMeasurement entity.
     *
     */
    public function showAction(UnitOfMeasurement $unitOfMeasurement)
    {
        $deleteForm = $this->createDeleteForm($unitOfMeasurement);

        return $this->render('unitofmeasurement/show.html.twig', array(
            'unitOfMeasurement' => $unitOfMeasurement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing UnitOfMeasurement entity.
     *
     */
    public function editAction(Request $request, UnitOfMeasurement $unitOfMeasurement)
    {
        $deleteForm = $this->createDeleteForm($unitOfMeasurement);
        $editForm = $this->createForm('AppBundle\Form\UnitOfMeasurementType', $unitOfMeasurement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($unitOfMeasurement);
            $em->flush();

            return $this->redirectToRoute('unit_edit', array('id' => $unitOfMeasurement->getId()));
        }

        return $this->render('unitofmeasurement/edit.html.twig', array(
            'unitOfMeasurement' => $unitOfMeasurement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a UnitOfMeasurement entity.
     *
     */
    public function deleteAction(Request $request, UnitOfMeasurement $unitOfMeasurement)
    {
        $form = $this->createDeleteForm($unitOfMeasurement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($unitOfMeasurement);
            $em->flush();
        }

        return $this->redirectToRoute('unit_index');
    }

    /**
     * Creates a form to delete a UnitOfMeasurement entity.
     *
     * @param UnitOfMeasurement $unitOfMeasurement The UnitOfMeasurement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UnitOfMeasurement $unitOfMeasurement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unit_delete', array('id' => $unitOfMeasurement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
