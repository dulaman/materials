<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class GroupOfMaterialsType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nazwa',
            ])
            ->add('parent', EntityType::class, [
                'label' => 'Element nadrzędny',
                'class' => 'AppBundle\Entity\GroupOfMaterials',
                'empty_data' => null,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('node')
                        ->orderBy('node.root, node.lft', 'ASC')
                    ->where('node.root = 1');
                },
                'required' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'preSetData']);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\GroupOfMaterials'
        ));
    }

    public function preSetData(FormEvent $event)
    {
        $group = $event->getData();
        $form = $event->getForm();

        if (!!$group && null !== $group->getId()) {
            $children = $this->em->getRepository('AppBundle:GroupOfMaterials')
                    ->getChildrenQueryBuilder($group)
                    ->select('node.id')->getQuery()->getArrayResult();
            $form->remove('parent')->add('parent', EntityType::class, [
                'label' => 'Element nadrzędny',
                'class' => 'AppBundle\Entity\GroupOfMaterials',
                'empty_data' => null,
                'query_builder' => function (EntityRepository $er) use ($group, $children) {
                    $query = $er->createQueryBuilder('node')
                        ->orderBy('node.root, node.lft', 'ASC')
                        ->where('node.root = 1')
                        ->andWhere('node.id NOT IN (:nodes)')
                        ->setParameter('nodes', array_merge([$group->getId()], $children));
                    return $query;
                },
                'required' => false,
            ]);
        }
    }
}
