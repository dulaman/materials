<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MaterialType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label' => 'Kod'
            ])
            ->add('name', null, [
                'label' => 'Nazwa'
            ])
            ->add('group', EntityType::class, [
                'label' => 'Grupa Materiałów',
                'class' => 'AppBundle\Entity\GroupOfMaterials',
                'empty_data' => null,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('node')
                        ->orderBy('node.root, node.lft', 'ASC')
                    ->where('node.root = 1');
                },
                'required' => false,
            ])
            ->add('unit', null, [
                'label' => 'Jednostka Miary'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Material'
        ));
    }
}
