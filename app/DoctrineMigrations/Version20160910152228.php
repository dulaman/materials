<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160910152228 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_of_materials ADD root_id INT DEFAULT NULL, ADD parent_id INT DEFAULT NULL, ADD lft INT NOT NULL, ADD lvl INT NOT NULL, ADD rgt INT NOT NULL');
        $this->addSql('ALTER TABLE group_of_materials ADD CONSTRAINT FK_4035FC8A79066886 FOREIGN KEY (root_id) REFERENCES group_of_materials (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_of_materials ADD CONSTRAINT FK_4035FC8A727ACA70 FOREIGN KEY (parent_id) REFERENCES group_of_materials (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_4035FC8A79066886 ON group_of_materials (root_id)');
        $this->addSql('CREATE INDEX IDX_4035FC8A727ACA70 ON group_of_materials (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_of_materials DROP FOREIGN KEY FK_4035FC8A79066886');
        $this->addSql('ALTER TABLE group_of_materials DROP FOREIGN KEY FK_4035FC8A727ACA70');
        $this->addSql('DROP INDEX IDX_4035FC8A79066886 ON group_of_materials');
        $this->addSql('DROP INDEX IDX_4035FC8A727ACA70 ON group_of_materials');
        $this->addSql('ALTER TABLE group_of_materials DROP root_id, DROP parent_id, DROP lft, DROP lvl, DROP rgt');
    }
}
