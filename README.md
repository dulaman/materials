Materials
=========

A Symfony project created on September 16, 2016, 8:42 pm by Adam Dulewicz.

Run run project you have to:
- `composer install`
- customize 'parameters.yml'
- configure Apache2
On Linux systems additionaly run following commands:

HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs

# Database

To create database and schema run following commands:

php app/console doctrine:database:create
php app/console doctrine:migrations:migrate

# Assets

To use assets run following commands:

php app/console assetic:dump --env=dev
php app/console assetic:dump --env=prod --no-debug


# Three party software
- https://github.com/Atlantic18/DoctrineExtensions
- http://getbootstrap.com/
- http://jquery.com/
and some other bundles created by sensiolabs
